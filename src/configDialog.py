# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/feupos/projects/ExaminationManager/ui/configDialog.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Settings(object):
    def setupUi(self, Settings):
        Settings.setObjectName("Settings")
        Settings.setEnabled(True)
        Settings.resize(277, 200)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(Settings.sizePolicy().hasHeightForWidth())
        Settings.setSizePolicy(sizePolicy)
        self.formLayout = QtWidgets.QFormLayout(Settings)
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(Settings)
        self.label.setObjectName("label")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.label)
        self.label_2 = QtWidgets.QLabel(Settings)
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(5, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.v_offset = QtWidgets.QDoubleSpinBox(Settings)
        self.v_offset.setDecimals(5)
        self.v_offset.setMinimum(-10.0)
        self.v_offset.setMaximum(10.0)
        self.v_offset.setSingleStep(1.0)
        self.v_offset.setObjectName("v_offset")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.v_offset)
        self.v_to_kgf = QtWidgets.QDoubleSpinBox(Settings)
        self.v_to_kgf.setMaximum(10000000.0)
        self.v_to_kgf.setObjectName("v_to_kgf")
        self.formLayout.setWidget(5, QtWidgets.QFormLayout.FieldRole, self.v_to_kgf)
        self.label_3 = QtWidgets.QLabel(Settings)
        self.label_3.setObjectName("label_3")
        self.formLayout.setWidget(6, QtWidgets.QFormLayout.LabelRole, self.label_3)
        self.buttonBox = QtWidgets.QDialogButtonBox(Settings)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.formLayout.setWidget(7, QtWidgets.QFormLayout.FieldRole, self.buttonBox)
        self.treshold = QtWidgets.QDoubleSpinBox(Settings)
        self.treshold.setDecimals(2)
        self.treshold.setObjectName("treshold")
        self.formLayout.setWidget(6, QtWidgets.QFormLayout.FieldRole, self.treshold)

        self.retranslateUi(Settings)
        self.buttonBox.accepted.connect(Settings.accept)
        self.buttonBox.rejected.connect(Settings.reject)
        QtCore.QMetaObject.connectSlotsByName(Settings)

    def retranslateUi(self, Settings):
        _translate = QtCore.QCoreApplication.translate
        Settings.setWindowTitle(_translate("Settings", "Load cell parameters"))
        self.label.setText(_translate("Settings", "Voltage offset"))
        self.label_2.setText(_translate("Settings", "Voltage to Kgf"))
        self.label_3.setText(_translate("Settings", "Treshold"))