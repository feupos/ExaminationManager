# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui\errordialog.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_errorDialog(object):
    def setupUi(self, errorDialog):
        errorDialog.setObjectName("errorDialog")
        errorDialog.resize(240, 100)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(errorDialog.sizePolicy().hasHeightForWidth())
        errorDialog.setSizePolicy(sizePolicy)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("error.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        errorDialog.setWindowIcon(icon)
        self.textField = QtWidgets.QLabel(errorDialog)
        self.textField.setGeometry(QtCore.QRect(11, 11, 221, 41))
        self.textField.setScaledContents(True)
        self.textField.setAlignment(QtCore.Qt.AlignCenter)
        self.textField.setWordWrap(True)
        self.textField.setIndent(-1)
        self.textField.setObjectName("textField")
        self.buttonBox = QtWidgets.QDialogButtonBox(errorDialog)
        self.buttonBox.setEnabled(True)
        self.buttonBox.setGeometry(QtCore.QRect(25, 60, 193, 28))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.buttonBox.sizePolicy().hasHeightForWidth())
        self.buttonBox.setSizePolicy(sizePolicy)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Close|QtWidgets.QDialogButtonBox.Retry)
        self.buttonBox.setCenterButtons(True)
        self.buttonBox.setObjectName("buttonBox")

        self.retranslateUi(errorDialog)
        self.buttonBox.accepted.connect(errorDialog.accept)
        self.buttonBox.rejected.connect(errorDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(errorDialog)

    def retranslateUi(self, errorDialog):
        _translate = QtCore.QCoreApplication.translate
        errorDialog.setWindowTitle(_translate("errorDialog", "Error"))
        self.textField.setText(_translate("errorDialog", "ERROR!\n"
"lol"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    errorDialog = QtWidgets.QDialog()
    ui = Ui_errorDialog()
    ui.setupUi(errorDialog)
    errorDialog.show()
    sys.exit(app.exec_())
