import os
import matplotlib.pyplot as plt
from fpdf import FPDF
import numpy as np
from numpy import arange, sin, pi
from scipy import signal
from scipy.signal import savgol_filter

# This gets the Qt stuff
from PyQt5 import QtWidgets, QtCore, QtGui
from PyQt5.QtWidgets import QApplication, QMainWindow
_translate = QtCore.QCoreApplication.translate

class PDF(FPDF):
    def header(self):
        pass
        # Logo
        #self.image('buffer1.png', 10, 8, 33)
        # Arial bold 15
        #self.set_font('Arial', 'B', 15)
        # Move to the right
        #self.cell(80)
        # Title
        #self.cell(30, 10, 'Title', 1, 0, 'C')
        # Line break
        #self.ln(20)

    # Page footer
    def footer(self):
        # Position at 1.5 cm from bottom
        self.set_y(-15)
        # Arial italic 8
        self.set_font('Arial', 'I', 8)
        # Page number
        self.cell(0, 10, 'Page ' + str(self.page_no()) + '/{nb}', 0, 0, 'C')

# Instantiation of inherited class

def pdfReport(fileName, data):
    pdf = PDF()
    pdf.alias_nb_pages()
    pdf.add_page()
    pdf.set_font('Times', '', 12)

    for element in data.textData:
        ptext = "{0}: {1}".format(_translate("MainWindow",element["label"]),element["data"])
        pdf.cell(0, 10, ptext, 0, 1)

    ptext = _translate("MainWindow","Comments") + ":\n" + data.comment
    pdf.cell(0, 10, ptext, 0, 1)

    n = 0
    for element in data.trialData:
        n = n+1
        fig, ax = plt.subplots()
        ax.plot(element["datasetx"], savgol_filter(element["datasety"],31,2))
        ax.axvline(x=element["stimuli"], color = "r")
        ax.axvline(x=element["response"], color = "b")
        plt.savefig("buffer.png")
        pdf.image("buffer.png",w = 200)
        plt.close()
        os.remove("buffer.png")

        responseTime = element["response"] - element["stimuli"]
        ptext = _translate('MainWindow','Response time :') + str(responseTime) + ' s'
        pdf.cell(0, 10, ptext, 0, 1)

    ptext = data.signature
    if ptext != "":
        pdf.cell(0, 20, "", 0, 1)
        pdf.line(50, pdf.get_y(), 210-50, pdf.get_y())
        pdf.cell(0, 10, ptext, 0, 1, align = 'C')


    pdf.output(fileName, 'F')
