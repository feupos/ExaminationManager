import matplotlib
import pyqtgraph as pg
import numpy as np
from numpy import arange, sin, pi
from scipy import signal
from scipy.signal import savgol_filter
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from PyQt5 import  QtWidgets, QtCore, QtGui
from analogDataSource import analogDataSource
from sessionData import sessionData
import threading
import json

dataLock = threading.Lock()


GAIN = 16
SAMPLE_RATE = 100

RESPONSE_VALUE = 20

class realTimePlotWidget(pg.PlotWidget):
    def __init__(self, window, aplication, t = 5):
        pg.PlotWidget.__init__(self)
        self.mainWindow = window
        self.app = aplication
        self.plotTimer = QtCore.QTimer(self)
        self.plotTimer.timeout.connect(self.updatePlot)
        self.dataSource = analogDataSource()
        self.dataSource.start()
        self.running = False
        self.runTrial = False
        self.timeLimit = t
        self.datasety = []
        self.datasetx = []
        self.treshold = 0
        #self.setMenuEnabled(enableMenu=False)
        self.curve = self.plot(self.datasetx, self.datasety, pen=(255,0,0))
        self.setYRange(0,60) #modify this
        self.plotTimer.start(100)
        self.b , self.a = signal.butter(10,15/(SAMPLE_RATE/2),btype = "lowpass")

    def start(self):
        self.datasety = []
        self.datasetx = []
        self.dataSource.running = True
        self.running = True

    def stop(self):
        self.running = False

    def startTrial(self):
        self.runTrial = True
        self.start()


    def updatePlot(self):
        dataLock.acquire(True)
        if (self.running == True):
            self.timeLimit = self.mainWindow.trialLengthSpinBox.value()
            #dataLock.acquire(True)
            self.datasety = self.datasety + self.dataSource.datasety
            self.dataSource.datasety = []

            if(len(self.datasety) >= SAMPLE_RATE*self.timeLimit+1):
                del self.datasety[0:len(self.datasety) - int(SAMPLE_RATE*self.timeLimit)]
                if(self.runTrial):
                    self.stop()
                    self.dataSource.stop()
                    self.dataSource.reset()
                    #ledStimuliOFF()
                    self.runTrial = False
                    self.datasetx = np.linspace(0,len(self.datasety)/SAMPLE_RATE, num = len(self.datasety))
                    self.savePlotData()

                    if(self.mainWindow.trialCounter.value() < self.mainWindow.numberOfTrialsSpinBox.value()):
                        self.mainWindow.nextTrialButton.setEnabled(True)
                    else:
                        self.mainWindow.finishButton.setEnabled(True)

            self.datasetx = np.linspace(0,len(self.datasety)/SAMPLE_RATE, num = len(self.datasety) ,endpoint = False)
            if (len(self.datasety) > 100):
                self.datafilt = savgol_filter(self.datasety,31,2)
                #self.datafilt = signal.filtfilt(self.b, self.a, self.datasety)
                self.curve.setData(self.datasetx, self.datafilt)
                self.setDownsampling(ds=True,auto=True,mode='subsample')
            #self.setDownsampling(mode='peak')
            self.setXRange(0,self.timeLimit)

            #dataLock.release()
            self.app.processEvents()
        dataLock.release()

    def savePlotData(self):
        with open('adjust.json', 'r') as file:
            data = json.load(file)
            self.treshold = data['treshold']
        response = self.datasetx[np.argmax(np.asarray(self.datasety) > self.treshold)]
        sessionData().addTrial(self.datasetx,self.datasety,self.mainWindow.stimuliOffsetSpinBox.value(), response)

    def recalibrate(self):
        self.dataSource.recalibrate()

    def __exit__(self):
        self.dataSource.stop()

class MyMplCanvas(FigureCanvas):
    """Ultimately, this is a QWidget (as well as a FigureCanvasAgg, etc.)."""
    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)

        self.compute_initial_figure()

        FigureCanvas.__init__(self, fig)
        self.setParent(parent)

        FigureCanvas.setSizePolicy(self,
                                   QtWidgets.QSizePolicy.Expanding,
                                   QtWidgets.QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

    def compute_initial_figure(self):
        pass

class MyStaticMplCanvas(MyMplCanvas):
    def compute(self, x = [], y = [], stimuli = 0, response = 0 ):
        self.datasetx = x
        self.datasety = y
        self.axes.plot(self.datasetx, self.datasety)
        if(stimuli):
            self.axes.axvline(x=stimuli, color = "r")
        if(response):
            self.axes.axvline(x=response, color = "b")
