import sys
import json

from errorDialog import Ui_errorDialog
from configDialog import Ui_Settings

from analogDataSource import saveSettings

from PyQt5 import QtCore, QtGui, QtWidgets

returnVal = -1

def setReturnVal(value):
    global returnVal
    returnVal = value

def errorPopUp(errorMsg):
    dialog = QtWidgets.QDialog()
    dialog.ui = Ui_errorDialog()
    dialog.ui.setupUi(dialog)
    dialog.ui.textField.setText(errorMsg)
    dialog.ui.buttonBox.accepted.connect(lambda: setReturnVal(0))
    dialog.ui.buttonBox.rejected.connect(lambda: setReturnVal(1))
    dialog.exec_()
    #dialog.show()
    return returnVal

def adjustPopUp():
    dialog = QtWidgets.QDialog()
    dialog.ui = Ui_Settings()
    dialog.ui.setupUi(dialog)
    with open('adjust.json', 'r') as file:
            data = json.load(file)
            dialog.ui.v_offset.setValue(data['v_offset'])
            dialog.ui.v_to_kgf.setValue(data['v_to_kgf'])
            dialog.ui.treshold.setValue(data['treshold'])
    dialog.ui.buttonBox.accepted.connect(lambda: saveSettings(dialog.ui.v_offset.value(),dialog.ui.v_to_kgf.value(),dialog.ui.treshold.value()))
    dialog.exec_()
    #dialog.show()
    return returnVal





if __name__ == "__main__":
    errorPopUp("test")
