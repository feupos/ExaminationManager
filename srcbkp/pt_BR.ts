<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="examinationManager.py" line="254"/>
        <source>Examination Manager</source>
        <translation>Gerenciador de Exames</translation>
    </message>
    <message>
        <location filename="examinationManager.py" line="255"/>
        <source>Patient</source>
        <translation>Paciente</translation>
    </message>
    <message>
        <location filename="examinationManager.py" line="256"/>
        <source>Date of birth</source>
        <translation>Data de nascimento</translation>
    </message>
    <message>
        <location filename="examinationManager.py" line="257"/>
        <source>ID number</source>
        <translation>Número de registro</translation>
    </message>
    <message>
        <location filename="examinationManager.py" line="258"/>
        <source>Insurance plan</source>
        <translation>Plano de saúde</translation>
    </message>
    <message>
        <location filename="examinationManager.py" line="259"/>
        <source>Requesting doctor</source>
        <translation>Médico requisitante</translation>
    </message>
    <message>
        <location filename="examinationManager.py" line="260"/>
        <source>Attending doctor</source>
        <translation>Médico aplicador</translation>
    </message>
    <message>
        <location filename="examinationManager.py" line="261"/>
        <source>Examination type</source>
        <translation>Tipo de exame</translation>
    </message>
    <message>
        <location filename="examinationManager.py" line="262"/>
        <source>Number of trials</source>
        <translation>Número de sessões</translation>
    </message>
    <message>
        <location filename="examinationManager.py" line="263"/>
        <source>Start</source>
        <translation>Iniciar</translation>
    </message>
    <message>
        <location filename="examinationManager.py" line="264"/>
        <source>Controls</source>
        <translation>Controles</translation>
    </message>
    <message>
        <location filename="examinationManager.py" line="265"/>
        <source>Trial</source>
        <translation>Sessão</translation>
    </message>
    <message>
        <location filename="examinationManager.py" line="266"/>
        <source>Trial Length</source>
        <translation>Duração</translation>
    </message>
    <message>
        <location filename="examinationManager.py" line="267"/>
        <source> s</source>
        <translation> s</translation>
    </message>
    <message>
        <location filename="examinationManager.py" line="268"/>
        <source>Stimuli offset</source>
        <translation>Estímulo</translation>
    </message>
    <message>
        <location filename="examinationManager.py" line="269"/>
        <source>Run Trial</source>
        <translation>Iniciar</translation>
    </message>
    <message>
        <location filename="examinationManager.py" line="270"/>
        <source>Next</source>
        <translation>Próximo</translation>
    </message>
    <message>
        <location filename="examinationManager.py" line="271"/>
        <source>Finish</source>
        <translation>Finalizar</translation>
    </message>
    <message>
        <location filename="examinationManager.py" line="272"/>
        <source>Report Options</source>
        <translation>Opções de relatório</translation>
    </message>
    <message>
        <location filename="pdfReport.py" line="56"/>
        <source>Comments</source>
        <translation>Comentários</translation>
    </message>
    <message>
        <location filename="examinationManager.py" line="274"/>
        <source>Generate report</source>
        <translation>Gerar realtório</translation>
    </message>
    <message>
        <location filename="examinationManager.py" line="275"/>
        <source>File Name</source>
        <translation>Nome do arquivo</translation>
    </message>
    <message>
        <location filename="examinationManager.py" line="276"/>
        <source>Menu</source>
        <translation>Menu</translation>
    </message>
    <message>
        <location filename="examinationManager.py" line="278"/>
        <source>Help</source>
        <translation type="obsolete">Ajuda</translation>
    </message>
    <message>
        <location filename="examinationManager.py" line="278"/>
        <source>About</source>
        <translation>Sobre</translation>
    </message>
    <message>
        <location filename="examinationManager.py" line="277"/>
        <source>Configuration</source>
        <translation>Configuração</translation>
    </message>
    <message>
        <location filename="examinationManager.py" line="279"/>
        <source>Shutdown</source>
        <translation>Desligar</translation>
    </message>
    <message>
        <location filename="examinationManager.py" line="280"/>
        <source>Recalibrate</source>
        <translation>Recalibrar</translation>
    </message>
</context>
<context>
    <name>errorDialog</name>
    <message>
        <location filename="main.py" line="142"/>
        <source>Unable to save files.
</source>
        <translation>Não foi possível salvar os arquivos.
</translation>
    </message>
    <message>
        <location filename="main.py" line="142"/>
        <source>Check if USB drive is connected!</source>
        <translation>Confira se o pen drive está conectado!</translation>
    </message>
    <message>
        <location filename="errorDialog.py" line="50"/>
        <source>Error</source>
        <translation>Erro</translation>
    </message>
    <message>
        <location filename="errorDialog.py" line="51"/>
        <source>ERROR!
lol</source>
        <translation>ERRO!
lol</translation>
    </message>
</context>
</TS>
