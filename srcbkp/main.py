# -*- coding: utf-8 -*-
"""
Created on Tue Aug 14 18:41:22 2018

@author: Feupos
"""

# needed packages
import sys
import random
import datetime
import threading
import time
import sched
import math
import functools
import multiprocessing
import csv
import os
from pathlib import Path

import numpy as np
# This gets the Qt stuff
from PyQt5 import QtWidgets, QtCore, QtGui
from PyQt5.QtWidgets import QApplication, QMainWindow
_translate = QtCore.QCoreApplication.translate
app = QApplication(sys.argv)

stub_gpio = False
try:
    import RPi.GPIO as IO
except:
    stub_gpio = True
    print("Unable to import RPi.GPIO library")

# This is our window from QtCreator
import examinationManager
from popUp import errorPopUp


# Additional files
from singleton import singleton
from sessionData import sessionData
from pdfReport import pdfReport
from analogDataSource import analogDataSource
from plotObjects import realTimePlotWidget, MyStaticMplCanvas
from reportManager import saveReport

LED_STIMULI_PIN = 40


# functions to control led stimuli

if(stub_gpio == False):
    IO.setmode(IO.BOARD)
    IO.setup(LED_STIMULI_PIN,IO.OUT)

def ledStimuliON():
    if(stub_gpio == False):
        IO.output(LED_STIMULI_PIN,1)
    else:
        print("LED ON")

def ledStimuliOFF():
    if(stub_gpio == False):
        IO.output(LED_STIMULI_PIN,0)
    else:
        print("LED OFF")

class mainWindow(QMainWindow, examinationManager.Ui_MainWindow):
 # access variables inside of the UI's file
    def __init__(self):
        translate = QtCore.QCoreApplication.translate

        super(self.__class__, self).__init__()
        self.setupUi(self) # gets defined in the UI file
        self.startButton.clicked.connect(self.startClick)
        self.runTrialButton.clicked.connect(self.runTrialClick)
        self.nextTrialButton.clicked.connect(self.nextTrialClick)
        self.finishButton.clicked.connect(self.finishClick)
        self.reportButton.clicked.connect(self.reportClick)
        self.examinationTypeComboBox.addItem("Brake")
        self.realTimePlot = realTimePlotWidget(self, app)
        self.graphLayout.addWidget(self.realTimePlot)
        self.actionShutdown.triggered.connect(shutdown)
        self.actionRecalibrate.triggered.connect(self.realTimePlot.recalibrate)
        self.showMaximized()
        self.showFullScreen()
        self.show()

    def startClick(self):
        #global currentSessionData
        sessionData().updateData(self)
        self.mainWidget.setCurrentIndex(1)
        self.runTrialButton.setEnabled(True)
        self.trialLengthSpinBox.setEnabled(True)
        self.nextTrialButton.setEnabled(False)
        self.finishButton.setEnabled(False)
        self.trialCounter.display(0)
        self.realTimePlot.start()

    def runTrialClick(self):
        self.runTrialButton.setEnabled(False)
        self.nextTrialButton.setEnabled(False)
        self.finishButton.setEnabled(False)
        self.trialCounter.display(1 + int(self.trialCounter.value()))
        QtCore.QTimer.singleShot(self.stimuliOffsetSpinBox.value()*1000, ledStimuliON)
        self.realTimePlot.startTrial()

    def nextTrialClick(self):
        ledStimuliOFF()
        self.runTrialButton.setEnabled(True)
        self.trialLengthSpinBox.setEnabled(True)
        self.nextTrialButton.setEnabled(False)
        self.realTimePlot.start()

    def finishClick(self):
        ledStimuliOFF()
        self.finishButton.setEnabled(False)
        self.mainWidget.setCurrentIndex(2)
        #currentSessionData.generatePDFReport()
        for data in sessionData().trialData:
            reportPlot = MyStaticMplCanvas()
            reportPlot.compute(data["datasetx"],data["datasety"],data["stimuli"])
            self.graphReportLayout.addWidget(reportPlot)

    def reportClick(self):
        self.setCursor(QtGui.QCursor(QtCore.Qt.WaitCursor))
        try:
            fileSaved = False
            error = 0
            if (len(self.fileNameLineEdit.text()) > 0):
                fileName = self.fileNameLineEdit.text()
            else:
                fileName = "report"
            sessionData().addComment(self.commentTextEdit.toPlainText())
            fileSaved = saveReport(fileName)

        except Exception as e:
            print(e)
            self.setEnabled(False)
            error = errorPopUp(_translate("errorDialog", "Unable to save files.\n")+
                               _translate("errorDialog", "Check if USB drive is connected!"))
            self.setEnabled(True)

        finally:
            self.setCursor(QtGui.QCursor(QtCore.Qt.ArrowCursor))
            if (fileSaved == True or error != 0):
                self.mainWidget.setCurrentIndex(0)
                for i in reversed(range(self.graphReportLayout.count())):
                    self.graphReportLayout.itemAt(i).widget().deleteLater()

#app = QApplication(sys.argv)
#form = mainWindow()

def shutdown():
    print("System shutdown\n")
    if sys.platform.startswith('linux'):
        os.system('sudo shutdown now')
    else:
        print("EXIT!\n")
        exit()


def main():
    try:
        print('Starting GUI')
        translator = QtCore.QTranslator()
        translator.load("src/pt_BR.qm")
        app.installTranslator(translator)
        window = mainWindow()
        sys.exit(app.exec_())
    except Exception as e:
        print(e)
    finally:
        pass

if __name__ == "__main__":
    main()
