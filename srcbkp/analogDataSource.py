try:
    import Adafruit_ADS1x15
except:
    print("Unable to import ADS1115 library")
import math
import threading
import time
import numpy as np


dataLock = threading.Lock()


# Create an ADS1115 ADC (16-bit) instance.
# Choose a gain of 1 for reading voltages from 0 to 4.09V.
# Or pick a different gain to change the range of voltages that are read:
#  - 2/3 = +/-6.144V
#  -   1 = +/-4.096V
#  -   2 = +/-2.048V
#  -   4 = +/-1.024V
#  -   8 = +/-0.512V
#  -  16 = +/-0.256V
# See table 3 in the ADS1015/ADS1115 datasheet for more info on gain.
#ADC_TO_VOLTAGE = 0.000125
ADC_TO_VOLTAGE_GAIN_16 = 0.0000078125
ADC_TO_VOLTAGE_GAIN_8 = 0.000015625
#VOLT_TO_KGF = 0.018
VOLT_TO_KGF = 2850   #2000 empirical values used
ZERO_ADJUST = 0.25175
GAIN = 8
SAMPLE_RATE = 100 #128


class analogDataSource(object):
    def __init__(self):
        print("Starting ADS1115 instance")
        #self.gain = g;
        #self.sample_rate = sr;
        self.running = False
        self.finished = False
        self.datasety = []
        self.datacount = 0
        self.timelimit = 0
        self.bias = 0
        self.gain = GAIN
        self.sample_rate = SAMPLE_RATE
        try:
            self.adc = Adafruit_ADS1x15.ADS1115()
            #self.adc.start_adc_difference(0, gain=GAIN, data_rate=SAMPLE_RATE)
            self.adc.start_adc(0, gain=GAIN, data_rate=128)
            self.STUBBED_MODE = False
        except:
            print("Running in stubbed mode")
            self.STUBBED_MODE = True
        self.running = True

        self.recalibrate()

        self.thread = threading.Thread(target=self.updateData, args=())
        self.thread.daemon = True                            # Daemonize thread
        self.thread.start()                                  # Start the execution


    def start(self, t = math.inf):
        self.running = True
        self.timelimit = t

    def reset(self, t = math.inf):
        self.datasety = []

    def stop(self):
        self.running = False

    def getData(self):
        dataLock.acquire(True)
        if(self.STUBBED_MODE == False):
            sample = (((self.adc.get_last_result()*ADC_TO_VOLTAGE_GAIN_8)-self.bias)*VOLT_TO_KGF)
        else:
            sample = self.bias + 2.5*(1+np.sin(2*np.pi*0.5*time.time()))

        dataLock.release()
        return sample

    def updateData(self):
        lostSamples = 0
        t = time.time()
        told = t
        while(self.finished == False):
            while(self.running == True):
                t = time.time()
                if (t-told > 1./self.sample_rate):
                    #dataLock.acquire(True)
                    self.datasety.append(self.getData())
                    #dataLock.release()
                    try:
                        if(t-told > 2*1./self.sample_rate):
                            raise Exception("Unable to sample data within time")
                    except:
                        lostSamples = lostSamples + 1
                        print("{0} lost samples".format(lostSamples))
                    told = t
            def __exit__(self):
                self.running = False
                self.finished = True

    def recalibrate(self):
        dataLock.acquire(True)
        bias = np.zeros(250)
        for i in range(250):
            time.sleep(0.004)

            if(self.STUBBED_MODE == False):
                bias[i] = self.adc.get_last_result()*ADC_TO_VOLTAGE_GAIN_8
            else:
                bias[i] = self.bias

        self.bias = np.mean(bias)
        dataLock.release()
