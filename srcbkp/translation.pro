SOURCES += main.py
SOURCES += examinationManager.py
SOURCES += popUp.py
SOURCES += errorDialog.py
SOURCES += reportManager.py
SOURCES += pdfReport.py
SOURCES += sessionData.py
SOURCES += singleton.py
TRANSLATIONS += pt_BR.ts
