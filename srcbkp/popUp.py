import sys

from errorDialog import Ui_errorDialog

from PyQt5 import QtCore, QtGui, QtWidgets

returnVal = -1

def setReturnVal(value):
    global returnVal
    returnVal = value

def errorPopUp(errorMsg):
    dialog = QtWidgets.QDialog()
    dialog.ui = Ui_errorDialog()
    dialog.ui.setupUi(dialog)
    dialog.ui.textField.setText(errorMsg)
    dialog.ui.buttonBox.accepted.connect(lambda: setReturnVal(0))
    dialog.ui.buttonBox.rejected.connect(lambda: setReturnVal(1))
    dialog.exec_()
    dialog.show()
    return returnVal

if __name__ == "__main__":
    errorPopUp("test")
