from pathlib import Path
import os
import sys

from sessionData import sessionData
from pdfReport import pdfReport

def saveReport(fileName = "report"):
    fileSaved = False
    n = 0
    if(sys.platform.startswith('linux') ):
        p = Path('/media')
        if(p.stat().st_size == 0):
            raise FileNotFoundError("Empty path")
        for child in p.iterdir():
            with open(str(child)+"/"+fileName+".csv", 'w', newline='') as csvfile:
                for data in sessionData().trialData:
                    n = n+1
                    csvfile.write("Trial {0} - stimuli at {1} seconds\n".format(n,data['stimuli']))
                    for x,y in zip(data['datasetx'], data['datasety']):
                        csvfile.write(str(x)+";"+str(y)+"\n")
                csvfile.close()
                print("File saved to "+str(child)+"/"+fileName+".csv")

        for child in p.iterdir():
            pdfReport(str(child)+"/"+fileName+".pdf",sessionData())
            print("File saved to "+str(child)+"/"+fileName+".pdf")
        fileSaved = True
    elif(sys.platform.startswith('win') ):
        p = Path.cwd()
        with open(str(p)+"\\reports\\"+fileName+".csv", 'w', newline='') as csvfile:
            for data in sessionData().trialData:
                n = n+1
                csvfile.write("Trial {0} - stimuli at {1} seconds\n".format(n,data['stimuli']))
                for x,y in zip(data['datasetx'], data['datasety']):
                    csvfile.write(str(x)+";"+str(y)+"\n")
            csvfile.close()
        pdfReport(str(p)+"\\reports\\"+fileName+".pdf",sessionData())
        fileSaved = True

    if(fileSaved == False):
        raise NameError("Unable to save files")

    return fileSaved
