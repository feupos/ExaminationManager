# -*- coding: utf-8 -*-
"""
Created on Wed Aug 15 19:39:21 2018

@author: Feupos
"""
# This class is a singleton to be manipulated during a sessionData

# This gets the Qt stuff
import PyQt5
from PyQt5.QtWidgets import *

# This is our window from QtCreator
import examinationManager
from singleton import singleton

@singleton
class sessionData(object):
    def __init__(self):
        self.trialData = []
        pass

    def updateData(self, form):
        print("Storing current session data")
        self.trialData = []
        self.textData = []
        self.comment = ""
        self.textData.append({"label":form.patientLabel.text(),"data":form.patientLineEdit.text()})
        self.textData.append({"label":form.dateOfBirthLabel.text(),"data":form.dateOfBirthDateEdit.text()})
        self.textData.append({"label":form.idNumberLabel.text(),"data":form.idNumberLineEdit.text()})
        self.textData.append({"label":form.insurancePlanLabel.text(),"data":form.insurancePlanLineEdit.text()})
        self.textData.append({"label":form.requestingDoctorLabel.text(),"data":form.requestingDoctorLineEdit.text()})
        self.textData.append({"label":form.attendingDoctorLabel.text(),"data":form.attendingDoctorLineEdit.text()})
        self.textData.append({"label":form.examinationTypeLabel.text(),"data":form.examinationTypeComboBox.currentText()})
        self.textData.append({"label":form.requestingDoctorLabel.text(),"data":form.requestingDoctorLineEdit.text()})


    def addTrial(self, x = [], y = [], s = 0):
        print("Storing current trial data")
        #self.trialData.append((datasetx,datasety, stimuli))
        self.trialData.append({"datasetx":x, "datasety":y, "stimuli":s})

    def addComment(self, s):
        self.comment = s
